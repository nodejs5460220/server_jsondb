import { readFile, writeFile } from 'node:fs/promises';

const FILEPATH = './posts.json';

function addPost(data) {
  return {
    id: Math.ceil(Math.random() * 10000000).toString(),
    authorId: data.authorId,
    photoUrl: data.photoUrl,
    likes: 0,
    text: data.text,
    createdAt: new Date().toISOString(),
    commentary: [],
  };
}

function mapParamsToPost(post, data) {
  return {
    ...post,
    ...(data.authorId && { authorId: data.authorId }),
    ...(data.photoUrl && { photoUrl: data.photoUrl }),
    ...(data.text && { text: data.text }),
  };
}

export class PostRepository {
  async readDataFromFile() {
    const json = await readFile(FILEPATH, { encoding: 'utf-8' });
    const data = await JSON.parse(json);
    return data;
  }

  getPosts() {
    return this.readDataFromFile();
  }

  async getPostById(id) {
    const posts = await this.readDataFromFile();
    const post = posts.find((post) => post.id === id);
    return post;
  }

  async createPost(data) {
    try {
      const allPosts = await this.getPosts();
      const post = addPost(data);
      const newPosts = [...allPosts, post];
      const json = JSON.stringify(newPosts);
      await writeFile(FILEPATH, json);
      return post;
    } catch (error) {
      console.log(error);
    }
  }

  async updatePost(id, data) {
    try {
      const oldPost = await this.getPostById(id);
      const updatedPost = mapParamsToPost(oldPost, data);
      const allPosts = await this.getPosts();
      const newPosts = allPosts.map((item) => {
        if (item.id === id) {
          return updatedPost;
        }
        return item;
      });
      const json = JSON.stringify(newPosts);
      await writeFile(FILEPATH, json);
      return updatedPost;
    } catch (error) {
      console.log(`update error : ${error}`);
    }
  }

  async deletePost(id) {
    try {
      const posts = await this.getPosts();
      const newPosts = posts.filter((item) => item.id !== id);
      const json = JSON.stringify(newPosts);
      await writeFile(FILEPATH, json);
    } catch (error) {
      console.log(`delete error : ${error}`);
    }
  }
}
