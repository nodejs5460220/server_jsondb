import express from 'express';
import { PostRepository } from './post.repository.js';
import bodyParser from 'body-parser';
import { router } from './controller.js';

const app = express();
const PORT = 3001;

app.use(bodyParser.urlencoded({ extended: true }));
app.use('/', router);

app.listen(PORT, () => {
  console.log('server start');
});
