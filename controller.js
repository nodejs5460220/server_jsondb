import express from 'express';
import { PostRepository } from './post.repository.js';

export const router = express.Router();
const postRepository = new PostRepository();

router.get('/posts', async (req, res) => {
  const posts = await postRepository.getPosts();
  // const data = JSON.stringify(posts);
  // res.send(`Get all posts : ${data}`);
  res.json(posts);
});

router.get('/posts/:postId', async (req, res) => {
  const post = await postRepository.getPostById(req.params.postId);
  if (post) {
    res.json(post);
  } else {
    res.sendStatus(404);
  }
});

router.post('/posts', async (req, res) => {
  try {
    const post = await postRepository.createPost(req.body);
    res.json(post);
  } catch (error) {
    res.sendStatus(500);
    console.log(`create post error:  ${error}`);
  }
});

router.patch('/posts/:postId', async (req, res) => {
  try {
    const post = await postRepository.updatePost(req.params.postId, req.body);
    res.json(post);
  } catch (error) {
    res.sendStatus(500);
    console.log(`patch post error:  ${error}`);
  }
});

router.delete('/posts/:postId', async (req, res) => {
  try {
    const post = await postRepository.deletePost(req.params.postId);
    res.json(post);
  } catch (error) {
    res.sendStatus(500);
    console.log(`delete post error:  ${error}`);
  }
});
